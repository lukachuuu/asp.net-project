﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;


namespace Vjezba.DAL
{
    public class WorkingOrderDTO
    {

        public WorkingOrderDTO()
        {
            this.ID = 0;
            this.Client = null;
            this.Car = null;
            this.Date = DateTime.Now;
            this.Request = null;
            this.Parts = new List<PartDTO>();
            this.Repairs = new List<RepairDTO>();
            this.Repairer = null;
        }

        public int ID { get; set; }
        [Required]
        public Client Client { get; set; }

        [Required]
        public Car Car { get; set; }

        [DisplayName("Datum izrade")]
        [Required(ErrorMessage = "Odaberite datum")]
        public DateTime Date { get; set; }

        [DisplayName("Zahtjev klijenta")]
        [Required(ErrorMessage = "Unesite zahtjev klijenta")]
        public string Request { get; set; }
        public List<PartDTO> Parts { get; set; }
        public List<RepairDTO> Repairs { get; set; }
        public int? RepairerID { get; set; }

        [DisplayName("Serviser")]
        public virtual Repairer Repairer { get; set; }
    }
}
