﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;
using System.Data.Entity;

namespace Vjezba.DAL
{
    public class WorkingOrderRepository : RepositoryBase<WorkingOrder>
    {
        public WorkingOrderRepository(CompaniesManagerDbContext dbContext)
            : base(dbContext)
        {
        }
        public WorkingOrderDTO GetDTO(int id)
        {
            var entity = DbContext.WorkingOrders
                .Include(p => p.Parts)
                .Include(p => p.Repairs)
                .Include(p => p.Car)
                .Include(p => p.Client)
                .Include(p => p.Repairer)
                .Where(p => p.ID == id)
                .FirstOrDefault();

            var dto = new WorkingOrderDTO();

            dto.ID = entity.ID;
            dto.Client = entity.Client;
            dto.Car = entity.Car;
            dto.Date = entity.Date;
            dto.Request = entity.Request;
            dto.Repairer = entity.Repairer;
            dto.RepairerID = entity.Repairer.ID;
            dto.Parts = new List<PartDTO>();
            dto.Repairs = new List<RepairDTO>();


            //Mapiranje dijelova
            foreach (var entityPart in entity.Parts)
            {
                var partDto = new PartDTO();
                partDto.Title = entityPart.Title;
                partDto.PartID = entityPart.ID;
                partDto.Id = entityPart.ID;

                dto.Parts.Add(partDto);

            }

            //Mapiranje popravka
            foreach (var entityRepair in entity.Repairs)
            {
                var repairDto = new RepairDTO();
                repairDto.Title = entityRepair.Title;
                repairDto.RepairID = entityRepair.ID;
                repairDto.Id = entityRepair.ID;

                dto.Repairs.Add(repairDto);

            }

            return dto;
        }

        public void InsertDTO(WorkingOrderDTO dto)
        {

            var entity = new WorkingOrder();
            entity.ID = dto.ID;
            entity.Car = dto.Car;
            entity.Client = dto.Client;
            entity.Date = dto.Date;
            entity.Repairer = dto.Repairer;
            entity.Request = dto.Request;

            entity.Parts = new List<Part>();


            foreach (var temp in dto.Parts)
            {
                var partTmp = new Part();
                partTmp.Title = temp.Title;
                partTmp.DateCreated = DateTime.Now;
                entity.Parts.Add(partTmp);
            }


            base.Insert(entity);
        }

        public void UpdateDTO(WorkingOrderDTO dto)
        {

            //dto.Part.Id ----> ako je novi 0, ako je stari onda je to id iz Part tablice


            var entity = DbContext.WorkingOrders
                .Include(p => p.Repairs)
                .Include(p => p.Car)
                .Include(p => p.Repairer)
                .Include(p => p.Parts)
                .Where(p => p.ID == dto.ID)
                .FirstOrDefault();


            //Mapiranje dto -> entitet

            entity.Car = dto.Car;
            entity.Client = dto.Client;
            entity.Date = dto.Date;
            entity.ID = dto.ID;
            entity.Repairer = dto.Repairer;
            entity.Request = dto.Request;

            var tmpDeletePart = new List<Part>();
            var tmpDeleteRepair = new List<Repair>();

            if (dto.Parts != null)
            {

                foreach (var dtoPart in dto.Parts)
                {
                    //novi dijelovi u WO
                    if (dtoPart.Id == 0)
                    {
                        var partTmp = new Part();
                        partTmp.ID = dtoPart.PartID;
                        partTmp.Title = dtoPart.Title;
                        partTmp.DateCreated = DateTime.Now;

                        //DbContext.Entry(partTmp).State = System.Data.Entity.EntityState.Added;
                        //entity.Parts.Add(partTmp);

                        using (var ctx = new CompaniesManagerDbContext())
                        {
                            int noOfRowInserted = ctx.Database.ExecuteSqlCommand("INSERT INTO WorkingOrderParts(WorkingOrder_ID,Part_ID)values(" + dto.ID + "," + dtoPart.PartID + ")");
                        }

                    }
                    //izmjena startih dijelova
                    else if (dtoPart.Id > 0)
                    {
                        //var entityPart = entity.Parts.Where(p => p.ID == dtoPart.Id).SingleOrDefault();
                        //entityPart.Title = dtoPart.Title;
                        //entityPart.DateModified = DateTime.Now;

                        using (var ctx = new CompaniesManagerDbContext())
                        {
                            int noOfRowUpdated = ctx.Database.ExecuteSqlCommand("Update WorkingOrderParts set Part_ID =" + dtoPart.PartID + " where Part_ID =" + dtoPart.Id + ";");

                        }

                    }

                }

                foreach (var entityParts in entity.Parts)
                {
                    if ((dto.Parts.Where(p => p.Id == entityParts.ID).FirstOrDefault()) == null)
                    {
                        tmpDeletePart.Add(entityParts);
                    }

                }

                foreach (var tmpD in tmpDeletePart)
                {
                    //DbContext.Entry(tmpD).State = System.Data.Entity.EntityState.Deleted;

                    using (var ctx = new CompaniesManagerDbContext())
                    {
                        int noOfRowDeleted = ctx.Database.ExecuteSqlCommand("DELETE FROM WorkingOrderParts WHERE Part_ID = " + tmpD.ID + ";");


                    }
                }
            }
            else
            {

                foreach (var entityPart in entity.Parts)
                {
                    tmpDeletePart.Add(entityPart);
                }

                foreach (var tmpD in tmpDeletePart)
                {
                    using (var ctx = new CompaniesManagerDbContext())
                    {
                        int noOfRowDeleted = ctx.Database.ExecuteSqlCommand("DELETE FROM WorkingOrderParts WHERE Part_ID = " + tmpD.ID + ";");


                    }
                }
            }


            //POPRAVCI

            if (dto.Repairs != null)
            {

                foreach (var dtoRepair in dto.Repairs)
                {
                    //novi popravci u WO
                    if (dtoRepair.Id == 0)
                    {
                        var repairTmp = new Repair();
                        repairTmp.ID = dtoRepair.RepairID;
                        repairTmp.Title = dtoRepair.Title;
                        repairTmp.DateCreated = DateTime.Now;


                        using (var ctx = new CompaniesManagerDbContext())
                        {
                            try
                            {
                                int noOfRowInserted = ctx.Database.ExecuteSqlCommand("INSERT INTO RepairWorkingOrders(WorkingOrder_ID,Repair_ID)values(" + dto.ID + "," + dtoRepair.RepairID + ")");
                            }
                            catch (Exception e)
                            {
                                int noOfRowDeleted = ctx.Database.ExecuteSqlCommand("DELETE FROM RepairWorkingOrders WHERE Repair_ID = " + dtoRepair.RepairID + ";");
                                int noOfRowInserted = ctx.Database.ExecuteSqlCommand("INSERT INTO RepairWorkingOrders(WorkingOrder_ID,Repair_ID)values(" + dto.ID + "," + dtoRepair.RepairID + ")");
                                Console.WriteLine(e.ToString());
                            }

                        }

                    }
                    //izmjena startih popravka
                    else if (dtoRepair.Id > 0)
                    {

                        using (var ctx = new CompaniesManagerDbContext())
                        {
                            int noOfRowUpdated = ctx.Database.ExecuteSqlCommand("Update RepairWorkingOrders set Repair_ID =" + dtoRepair.RepairID + " where Repair_ID =" + dtoRepair.Id + ";");
                        }
                    }

                }

                foreach (var entityRepair in entity.Repairs)
                {
                    if ((dto.Repairs.Where(p => p.Id == entityRepair.ID).FirstOrDefault()) == null)
                    {
                        tmpDeleteRepair.Add(entityRepair);
                    }
                }

                foreach (var tmpD in tmpDeletePart)
                {
                    //DbContext.Entry(tmpD).State = System.Data.Entity.EntityState.Deleted;

                    using (var ctx = new CompaniesManagerDbContext())
                    {
                        int noOfRowDeleted = ctx.Database.ExecuteSqlCommand("DELETE FROM RepairWorkingOrders WHERE Repair_ID = " + tmpD.ID + ";");
                    }
                }
            }
            else
            {

                foreach (var entityRepair in entity.Repairs)
                {
                    tmpDeleteRepair.Add(entityRepair);
                }

                foreach (var tmpD in tmpDeleteRepair)
                {
                    using (var ctx = new CompaniesManagerDbContext())
                    {
                        int noOfRowDeleted = ctx.Database.ExecuteSqlCommand("DELETE FROM RepairWorkingOrders WHERE Repair_ID = " + tmpD.ID + ";");
                    }
                }
            }

            base.Update(entity);

        }
    }

}
