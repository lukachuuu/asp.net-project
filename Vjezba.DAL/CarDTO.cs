﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;

namespace Vjezba.DAL
{
    public class CarDTO
    {
        public CarDTO()
        {

        }
        public int Id { get; set; }

        [DisplayName("Marka vozila")]
        [Required(ErrorMessage = "Unesite marku vozila")]
        public string Mark { get; set; }

        [DisplayName("Tip vozila")]
        [Required(ErrorMessage = "Unesite tip vozila")]
        public string Type { get; set; }

        [DisplayName("Godina proizvodnje")]
        [Required(ErrorMessage = "Unesite godinu proizvodnje")]
        [RegularExpression("^[0-9]{4}$", ErrorMessage = "Neispravan unos")]
        public string Year { get; set; }

        [DisplayName("Oznaka registarskih tablica")]
        [Required(ErrorMessage = "Unesite registarske oznake")]
        [RegularExpression("[a-zA-Z]{2}-[0-9]{3,4}-[a-zA-Z]{2}$", ErrorMessage = "Neispravan unos")]
        public string LicensePlates { get; set; }

        [DisplayName("Broj kilometara")]
        [Required(ErrorMessage = "Unesite broj kilometara vozila")]
        [RegularExpression("^[0-9]{0,8}$", ErrorMessage = "Neispravan unos")]
        public int NumOfKm { get; set; }

        [DisplayName("Serijski broj vozila")]
        public string SerialNum { get; set; }
        public int ClientID { get; set; }
        public virtual Client Client { get; set; }
    }
}
