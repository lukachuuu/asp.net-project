﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;
using System.Data.Entity;

namespace Vjezba.DAL
{
    public class ClientRepository : RepositoryBase<Client>
    {
        public ClientRepository(CompaniesManagerDbContext db)
            : base(db)
        {
        }

        public ClientDTO GetDTO(int id)
        {
            var entity = DbContext.Clients
                .Include(p => p.Cars)
                .Where(p => p.ID == id)
                .FirstOrDefault();

            var dto = new ClientDTO();
            dto.Id = entity.ID;
            dto.Name = entity.Name;
            dto.Oib = entity.Oib;
            dto.Phone = entity.Phone;
            dto.Address = entity.Address;
            dto.Citiy = entity.Citiy;
            dto.Surname = entity.Surname;
            dto.Cars = new List<CarDTO>();

          
            //Mapiranje vozila
            foreach (var entityCar in entity.Cars)
            {
                var carDto = new CarDTO();
                carDto.Id = entityCar.ID;
                carDto.Mark = entityCar.Mark;
                carDto.NumOfKm = entityCar.NumOfKm;
                carDto.LicensePlates = entityCar.LicensePlates;
                carDto.SerialNum = entityCar.SerialNum;
                carDto.Type = entityCar.Type;
                carDto.Year = entityCar.Year;
                carDto.Client = entityCar.Client;
                carDto.ClientID = entityCar.ClientID;
                dto.Cars.Add(carDto);

            }

            return dto;
        }

        public void InsertDTO(ClientDTO dto)
        {
            var entity = new Client();
            entity.Name = dto.Name;
            entity.Citiy = dto.Citiy;
            entity.Oib = dto.Oib;
            entity.Phone = dto.Phone;
            entity.Surname = dto.Surname;
            entity.ID = dto.Id;
            entity.Address = dto.Address;
            

            entity.Cars = new List<Car>();


            foreach (var temp in dto.Cars)
            {
                var carTmp = new Car();
                carTmp.DateCreated = DateTime.Now;
                carTmp.ID = temp.Id;
                carTmp.LicensePlates = temp.LicensePlates;
                carTmp.Mark = temp.Mark;
                carTmp.Type = temp.Type;
                carTmp.NumOfKm = temp.NumOfKm;
                carTmp.SerialNum = temp.SerialNum;
                carTmp.Client = temp.Client;
                carTmp.ClientID = temp.ClientID;
                carTmp.Year = temp.Year;

                entity.Cars.Add(carTmp);

            }

            base.Insert(entity);
        }

        public void UpdateDTO(ClientDTO dto)
        {
            var entity = DbContext.Clients
                .Include(p => p.Cars)
                .Where(p => p.ID == dto.Id)
                .FirstOrDefault();

            //Mapiranje dto -> entitet
            entity.Name = dto.Name;
            entity.Citiy = dto.Citiy;
            entity.Oib = dto.Oib;
            entity.Phone = dto.Phone;
            entity.Surname = dto.Surname;
            entity.ID = dto.Id;
            entity.Address = dto.Address;

            var cars = new List<Car>();
            var tmpDelete = new List<Car>();

            if (dto.Cars != null)
            {

                foreach (var dtoCar in dto.Cars)
                {

                    if (dtoCar.Id == 0)
                    {
                        var carTmp = new Car();
                        carTmp.DateCreated = DateTime.Now;
                        carTmp.ID = dtoCar.Id;
                        carTmp.LicensePlates = dtoCar.LicensePlates;
                        carTmp.Mark = dtoCar.Mark;
                        carTmp.Type = dtoCar.Type;
                        carTmp.NumOfKm = dtoCar.NumOfKm;
                        carTmp.SerialNum = dtoCar.SerialNum;
                        carTmp.Client = dtoCar.Client;
                        carTmp.ClientID = dtoCar.ClientID;
                        carTmp.Year = dtoCar.Year; //datepicker

                        entity.Cars.Add(carTmp);


                    }

                    else if (dtoCar.Id > 0)
                    {
                        var entityCar = entity.Cars.Where(p => p.ID == dtoCar.Id).SingleOrDefault();
                        entityCar.LicensePlates = dtoCar.LicensePlates;
                        entityCar.Mark = dtoCar.Mark;
                        entityCar.NumOfKm = dtoCar.NumOfKm;
                        entityCar.SerialNum = dtoCar.SerialNum;
                        entityCar.Type = dtoCar.Type;
                        entityCar.Year = dtoCar.Year;

                    }

                }

                foreach (var entityCars in entity.Cars)
                {
                    if ((dto.Cars.Where(p => p.Id == entityCars.ID).FirstOrDefault()) == null)
                    {
                        tmpDelete.Add(entityCars);
                    }
                }

                foreach (var tmpD in tmpDelete)
                {
                    DbContext.Entry(tmpD).State = System.Data.Entity.EntityState.Deleted;
                }

            }
            else
            {

                foreach (var entityCars in entity.Cars)
                {               
                   tmpDelete.Add(entityCars);
                }                

                foreach (var tmpD in tmpDelete)
                {
                    DbContext.Entry(tmpD).State = System.Data.Entity.EntityState.Deleted;
                }
            }

            base.Update(entity);

        }
    }

}
