﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;

namespace Vjezba.DAL
{
    public class ClientDTO
    {
        public ClientDTO()
        {
            this.Id = 0;
            this.Name = null;
            this.Surname = null;
            this.Phone = null;
            this.Oib = null;
            this.Address = null;
            this.Citiy = null;
            this.Cars = new List<CarDTO>();
        }

        public int Id { get; set; }

        [DisplayName("Ime")]
        [Required(ErrorMessage = "Unesite ime klijenta")]
        public string Name { get; set; }

        [DisplayName("Prezime")]
        public string Surname { get; set; }

        [DisplayName("Adresa")]
        [Required(ErrorMessage = "Unesite adresu servisera")]
        public string Address { get; set; }

        [DisplayName("OIB")]
        [Required(ErrorMessage = "Unesite OIB servisera")]
        [RegularExpression("(^[0-9]{11}$)", ErrorMessage = "Neispravan OIB, unesite ponovno")]
        public string Oib { get; set; }

        [DisplayName("Grad")]
        public string Citiy { get; set; }

        [DisplayName("Broj telefona")]
        [Required(ErrorMessage = "Unesite kontakt")]
        public string Phone { get; set; }
        public List<CarDTO> Cars { get; set; }
    }
}
