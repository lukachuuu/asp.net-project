﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;

namespace Vjezba.DAL
{
    public class CarRepository : RepositoryBase<Car>
    {
        public CarRepository(CompaniesManagerDbContext db) : base(db)
        {
        }
    }

}
