﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;

namespace Vjezba.DAL
{
    public class PartRepository: RepositoryBase<Part>
    {
        public PartRepository(CompaniesManagerDbContext db) : base(db)
        {
        }
    }

}
