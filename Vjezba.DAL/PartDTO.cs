﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Vjezba.Model;

namespace Vjezba.DAL
{
    public class PartDTO
    {
        public int Id { get; set; }
        public int PartID { get; set; }

        [DisplayName("Naziv")]
        [Required(ErrorMessage = "Odaberite")]
        public string Title { get; set; }
        public WorkingOrder WorkingOrder { get; set; }
        public virtual List<WorkingOrder> WorkingOrders { get; set; }
        public int WorkingOrderID { get; set; }

    }
}
