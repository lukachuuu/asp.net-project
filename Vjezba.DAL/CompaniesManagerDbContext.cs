﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using Vjezba.Model;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Vjezba.DAL
{
    public class CompaniesManagerDbContext : IdentityDbContext<User> 
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Repair> Repairs { get; set; }
        public DbSet<WorkingOrder> WorkingOrders { get; set; }
        public DbSet<Part> Parts { get; set; }
        public DbSet<Repairer> Repairer { get; set; }

        public CompaniesManagerDbContext()
            : base("CompaniesManagerDbContext", throwIfV1Schema: false)
        {

        }

        public static CompaniesManagerDbContext Create()
        {
            return new CompaniesManagerDbContext();
        }


        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    //modelBuilder.Entity<WorkingOrder>().HasMany(p => p.Parts).WithMany().Map(x =>
        //    //{
        //    //    x.MapLeftKey("WorkingOrder_Id");
        //    //    x.MapRightKey("Part_Id");
        //    //    x.ToTable("WorkingOrderParts");
        //    //}); 

        //    modelBuilder.Entity<WorkingOrder>().HasMany(p => p.Repairs).WithMany();
        //    modelBuilder.Entity<WorkingOrder>().HasMany(p => p.Parts).WithMany();

        //}   

    }

   

}