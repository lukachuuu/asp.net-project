namespace Vjezba.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init6 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cars", "Mark", c => c.String(nullable: false));
            AlterColumn("dbo.Cars", "Type", c => c.String(nullable: false));
            AlterColumn("dbo.Cars", "Year", c => c.String(nullable: false));
            AlterColumn("dbo.Cars", "LicensePlates", c => c.String(nullable: false));
            AlterColumn("dbo.Clients", "Name", c => c.String(nullable: false));
            AlterColumn("dbo.Clients", "Address", c => c.String(nullable: false));
            AlterColumn("dbo.Clients", "Oib", c => c.String(nullable: false));
            AlterColumn("dbo.Clients", "Phone", c => c.String(nullable: false));
            AlterColumn("dbo.Parts", "Title", c => c.String(nullable: false));
            AlterColumn("dbo.Repairers", "Oib", c => c.String(nullable: false));
            AlterColumn("dbo.Repairs", "Title", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Repairs", "Title", c => c.String());
            AlterColumn("dbo.Repairers", "Oib", c => c.String());
            AlterColumn("dbo.Parts", "Title", c => c.String());
            AlterColumn("dbo.Clients", "Phone", c => c.String());
            AlterColumn("dbo.Clients", "Oib", c => c.String());
            AlterColumn("dbo.Clients", "Address", c => c.String());
            AlterColumn("dbo.Clients", "Name", c => c.String());
            AlterColumn("dbo.Cars", "LicensePlates", c => c.String());
            AlterColumn("dbo.Cars", "Year", c => c.String());
            AlterColumn("dbo.Cars", "Type", c => c.String());
            AlterColumn("dbo.Cars", "Mark", c => c.String());
        }
    }
}
