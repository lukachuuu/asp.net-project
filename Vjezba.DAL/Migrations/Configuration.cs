﻿namespace Vjezba.DAL.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using Vjezba.Model;

    internal sealed class Configuration : DbMigrationsConfiguration<Vjezba.DAL.CompaniesManagerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Vjezba.DAL.CompaniesManagerDbContext context)
        {
          

            context.Parts.AddOrUpdate(
                p => p.Title,
                new Part { Title = "Zupčasti remen", DateCreated = DateTime.Now},
                new Part { Title = "Stazni remen", DateCreated = DateTime.Now},
                new Part { Title = "Prednje disk pločice", DateCreated = DateTime.Now},
                new Part { Title = "Zadnje disk pločice", DateCreated = DateTime.Now},
                new Part { Title = "Prednji diskovi", DateCreated = DateTime.Now},
                new Part { Title = "Zadnji diskovi", DateCreated = DateTime.Now},
                new Part { Title = "Ulje 10W40", DateCreated = DateTime.Now},
                new Part { Title = "Kočiono ulje", DateCreated = DateTime.Now},
                new Part { Title = "Ležajevi kotača", DateCreated = DateTime.Now},
                new Part { Title = "Filter ulja", DateCreated = DateTime.Now},
                new Part { Title = "Filter zraka", DateCreated = DateTime.Now},
                new Part { Title = "Filter klime", DateCreated = DateTime.Now}
                );

            context.Repairs.AddOrUpdate(
                p => p.Title,
                new Repair { Title = "Zamjena ulja", DateCreated = DateTime.Now },
                new Repair { Title = "Brušenje glave", DateCreated = DateTime.Now },
                new Repair { Title = "Punjenje klime", DateCreated = DateTime.Now },
                new Repair { Title = "Zamjena disk pločica", DateCreated = DateTime.Now },
                new Repair { Title = "Zamjena uljnog filtera", DateCreated = DateTime.Now },
                new Repair { Title = "Zamjena zupčastog remena", DateCreated = DateTime.Now },
                new Repair { Title = "Zamjena guma", DateCreated = DateTime.Now },
                new Repair { Title = "Servis", DateCreated = DateTime.Now },
                new Repair { Title = "Podmazivanje ležajeva kotača", DateCreated = DateTime.Now },
                new Repair { Title = "Kontrola kočionog ulja", DateCreated = DateTime.Now },
                new Repair { Title = "Zamjena diskova", DateCreated = DateTime.Now },
                new Repair { Title = "Tokarenje diskova", DateCreated = DateTime.Now }

                );

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!roleManager.RoleExists("Admin"))
            {
                roleManager.Create(new IdentityRole("Admin"));
            }

            if (!roleManager.RoleExists("User"))
            {
                roleManager.Create(new IdentityRole("User"));
            }

            if (!roleManager.RoleExists("Manager"))
            {
                roleManager.Create(new IdentityRole("Manager"));
            }
                              
        }
    }
}
