namespace Vjezba.DAL.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class init2 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Parts", "WorkingOrder_ID", "dbo.WorkingOrders");
            DropForeignKey("dbo.Repairs", "WorkingOrder_ID", "dbo.WorkingOrders");
            DropIndex("dbo.Parts", new[] { "WorkingOrder_ID" });
            DropIndex("dbo.Repairs", new[] { "WorkingOrder_ID" });
            CreateTable(
                "dbo.WorkingOrderParts",
                c => new
                    {
                        WorkingOrder_ID = c.Int(nullable: false),
                        Part_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.WorkingOrder_ID, t.Part_ID })
                .ForeignKey("dbo.WorkingOrders", t => t.WorkingOrder_ID, cascadeDelete: true)
                .ForeignKey("dbo.Parts", t => t.Part_ID, cascadeDelete: true)
                .Index(t => t.WorkingOrder_ID)
                .Index(t => t.Part_ID);
            
            CreateTable(
                "dbo.RepairWorkingOrders",
                c => new
                    {
                        Repair_ID = c.Int(nullable: false),
                        WorkingOrder_ID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Repair_ID, t.WorkingOrder_ID })
                .ForeignKey("dbo.Repairs", t => t.Repair_ID, cascadeDelete: true)
                .ForeignKey("dbo.WorkingOrders", t => t.WorkingOrder_ID, cascadeDelete: true)
                .Index(t => t.Repair_ID)
                .Index(t => t.WorkingOrder_ID);
            
            DropColumn("dbo.Parts", "WorkingOrder_ID");
            DropColumn("dbo.Repairs", "WorkingOrder_ID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Repairs", "WorkingOrder_ID", c => c.Int());
            AddColumn("dbo.Parts", "WorkingOrder_ID", c => c.Int());
            DropForeignKey("dbo.RepairWorkingOrders", "WorkingOrder_ID", "dbo.WorkingOrders");
            DropForeignKey("dbo.RepairWorkingOrders", "Repair_ID", "dbo.Repairs");
            DropForeignKey("dbo.WorkingOrderParts", "Part_ID", "dbo.Parts");
            DropForeignKey("dbo.WorkingOrderParts", "WorkingOrder_ID", "dbo.WorkingOrders");
            DropIndex("dbo.RepairWorkingOrders", new[] { "WorkingOrder_ID" });
            DropIndex("dbo.RepairWorkingOrders", new[] { "Repair_ID" });
            DropIndex("dbo.WorkingOrderParts", new[] { "Part_ID" });
            DropIndex("dbo.WorkingOrderParts", new[] { "WorkingOrder_ID" });
            DropTable("dbo.RepairWorkingOrders");
            DropTable("dbo.WorkingOrderParts");
            CreateIndex("dbo.Repairs", "WorkingOrder_ID");
            CreateIndex("dbo.Parts", "WorkingOrder_ID");
            AddForeignKey("dbo.Repairs", "WorkingOrder_ID", "dbo.WorkingOrders", "ID");
            AddForeignKey("dbo.Parts", "WorkingOrder_ID", "dbo.WorkingOrders", "ID");
        }
    }
}
