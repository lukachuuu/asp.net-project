﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba.Model
{
    public class Repairer : EntityBase
    {
        [DisplayName("Ime")]
        [Required(ErrorMessage = "Unesite ime servisera")]
        public String Name { get; set; }

        [DisplayName("Prezime")]
        public String Surname { get; set; }

        [DisplayName("OIB")]
        [Required(ErrorMessage = "Unesite OIB servisera")]
        [RegularExpression("(^[0-9]{11}$)", ErrorMessage = "Neispravan OIB, unesite ponovno")]
        public String Oib { get; set; }
    }
}
