﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba.Model
{
    public class WorkingOrder : EntityBase
    {
        public WorkingOrder()
        {
            this.Parts = new HashSet<Part>();
            this.Repairs = new HashSet<Repair>();
        }

        [ForeignKey("Client")]
        public int? ClientID { get; set; }

        [DisplayName("Klijent")]
        public Client Client {get; set;}

        [ForeignKey("Car")]
        public int? CarID { get; set; }

        [DisplayName("Vozilo")]
        public Car Car { get; set;}

        [DisplayName("Datum i vrijeme radnog naloga")]
        public DateTime Date { get; set; }

        [DisplayName("Zahjevi klijenta")]
        public string Request { get; set; }
        public virtual ICollection<Part> Parts { get; set; }
        public virtual ICollection<Repair> Repairs { get; set; }

        [ForeignKey("Repairer")]
        public int? RepairerID { get; set; }
        public virtual Repairer Repairer { get; set; }


    }
}
