﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba.Model
{
    public class Client : EntityBase
    {
        [DisplayName("Ime")]
        [Required(ErrorMessage = "Unesite ime klijenta")]
        public string Name { get; set; }

        [DisplayName("Prezime")]
        public string Surname { get; set; }

        [DisplayName("Adresa")]
        [Required(ErrorMessage = "Unesite adresu servisera")]
        public string Address { get; set; }

        [DisplayName("OIB")]
        [Required(ErrorMessage = "Unesite OIB servisera")]
        [RegularExpression("(^[0-9]{11}$)", ErrorMessage = "Neispravan OIB, unesite ponovno")]
        public string Oib { get; set; }

        [DisplayName("Grad")]
        public string Citiy { get; set; }

        [DisplayName("Broj telefona")]
        [Required(ErrorMessage = "Unesite kontakt")]
        public string Phone { get; set; }
        public virtual ICollection<Car> Cars { get; set; }


    }
}
