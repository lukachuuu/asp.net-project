﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba.Model
{
    public class Part : EntityBase
    {
        public Part()
        {
            this.WorkingOrders = new HashSet<WorkingOrder>();
        }
        public virtual ICollection<WorkingOrder> WorkingOrders { get; set; }

        [DisplayName("Naziv")]
        [Required(ErrorMessage = "Unesite naziv")]
        public string Title { get; set; }

    }
}
