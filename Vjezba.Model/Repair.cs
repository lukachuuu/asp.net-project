﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Vjezba.Model
{
    public class Repair : EntityBase
    {
        public Repair()
        {
            this.WorkingOrders = new HashSet<WorkingOrder>();
        }
        public virtual ICollection<WorkingOrder> WorkingOrders { get; set; }

        [DisplayName("Naziv popravka")]
        [Required(ErrorMessage = "Unesite naziv popravka")]
        public string Title { get; set; }
    }
}
