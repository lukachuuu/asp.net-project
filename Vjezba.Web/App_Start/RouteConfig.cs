﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Vjezba.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {

            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapMvcAttributeRoutes();

            routes.MapRoute(
                name: "klijent_detalj",
                  url: "klijent/detalji/{id}",
                  defaults: new { controller = "Client", action = "Details" },
                  constraints: new { id = "[0-9]+" }
            );


            routes.MapRoute(
                name: "klijent_index",
                url: "klijenti",
                defaults: new { controller = "Client", action = "Index" }

            );



            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
