﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Model;

namespace Vjezba.Web.Controllers
{
    public class PartController : VjezbaControllerBase
    {
        // GET: Part
        [Authorize]
        public ActionResult Index()
        {
            var model = PartRepository.All.ToList();

            return View(model);
        }

        [Authorize]
        public ActionResult Details(int id)
        {
            var model = PartRepository.Find(id);

            return View(model);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create(Part model)
        {
            if (ModelState.IsValid)
            {
                PartRepository.Insert(model);
                PartRepository.Save();
                return RedirectToAction("Create");
            }

            return View(model);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int id)
        {

            var model = PartRepository.Find(id);

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(Part formModel)
        {
            if (ModelState.IsValid)
            {
                var modelDb = PartRepository.Find(formModel.ID);
                if (this.TryUpdateModel(modelDb))
                {
                    PartRepository.Update(modelDb);
                    PartRepository.Save();

                    return RedirectToAction("Index");
                }
            }
            return View(formModel);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int id)
        {
            PartRepository.Delete(id);
            PartRepository.Save();

            return RedirectToAction("Index");
        }
    }
}