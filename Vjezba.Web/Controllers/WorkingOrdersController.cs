﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Vjezba.DAL;
using Vjezba.Model;

namespace Vjezba.Web.Controllers
{
    public class WorkingOrdersController : VjezbaControllerBase
    {

        // GET: WorkingOrders
        public ActionResult Index()
        {

            var model = WorkingOrderRepository.All
                .Include(p => p.Car)
                .Include(p => p.Client)
                .Include(p => p.Repairer)
                .Include(p => p.Client)
                .ToList();

            return View(model);
        }

        // GET: WorkingOrders/Details/5
        public ActionResult Details(int id)
        {

            var details = WorkingOrderRepository.All
                 .Include(p => p.Car)
                 .Include(p => p.Client)
                 .Include(p => p.Repairer)
                 .Include(p => p.Client)
                 .Where(m => m.ID == id)
                 .SingleOrDefault();

            return View(details);
        }

        // GET: WorkingOrders/Create
        public ActionResult Create()
        {
            FillDropdownValuesRepairers();
            FillDropdownValuesParts();
            FillDropdownValuesRepairs();
            return View(new WorkingOrderDTO());
        }

        [HttpPost]
        public ActionResult Create(WorkingOrderDTO model, String car, String client)
        {


            Car tmpCar = new Car();
            tmpCar = CarRepository.Find(Int32.Parse(car));
            model.Car = tmpCar;

            Client tmpClient = new Client();
            tmpClient = ClientRepository.Find(Int32.Parse(client));
            model.Client = tmpClient;

            Repairer tmpRepairer = new Repairer();
            int idServ = model.RepairerID.GetValueOrDefault();
            tmpRepairer = RepairerRepository.Find(idServ);
            model.Repairer = tmpRepairer;

            WorkingOrder tmpWorkingOrder = new WorkingOrder();
            tmpWorkingOrder.Parts = new List<Part>();
            tmpWorkingOrder.Repairs = new List<Repair>();

            List<PartDTO> tmpDtoParts = new List<PartDTO>();

            foreach (var part in model.Parts)
            {
                var tmp = new Part();
                tmp = PartRepository.Find(part.PartID);
                tmp.DateCreated = DateTime.Now;
                tmpWorkingOrder.Parts.Add(tmp);
            }

            foreach (var repair in model.Repairs)
            {
                var tmp = new Repair();
                tmp = RepairRepository.Find(repair.RepairID);
                tmp.DateCreated = DateTime.Now;
                tmpWorkingOrder.Repairs.Add(tmp);
            }

            foreach (var part in tmpWorkingOrder.Parts)
            {
                var tmp = new PartDTO();
                tmp.Title = part.Title;
                tmp.Id = part.ID;
                tmpDtoParts.Add(tmp);
            }

            model.Parts = tmpDtoParts;

            tmpWorkingOrder.Repairer = tmpRepairer;
            tmpWorkingOrder.Client = tmpClient;
            tmpWorkingOrder.Car = tmpCar;
            tmpWorkingOrder.Request = model.Request;
            tmpWorkingOrder.Date = model.Date;
            tmpWorkingOrder.DateCreated = DateTime.Now;
            tmpWorkingOrder.CarID = tmpCar.ID;


            WorkingOrderRepository.Insert(tmpWorkingOrder);
            WorkingOrderRepository.Save();
            return RedirectToAction("Index");

        }

        // GET: WorkingOrders/Edit/5
        public ActionResult Edit(int id)
        {
            FillDropdownValuesParts();
            FillDropdownValuesRepairers();
            FillDropdownValuesClients();
            FillDropdownValuesRepairs();

            var model = WorkingOrderRepository.GetDTO(id);

            return View(model);
        }

        // POST: WorkingOrders/Edit/5
        [HttpPost]
        public ActionResult Edit(WorkingOrderDTO model, String car, String client)
        {

            model.Car = CarRepository.Find(Int32.Parse(car));
            model.Client = ClientRepository.Find(Int32.Parse(client));
            model.Repairer = RepairerRepository.Find(model.RepairerID.GetValueOrDefault());

            //model je sad oke


            Repairer tmpRepairer = new Repairer();
            int idServ = model.RepairerID.GetValueOrDefault();
            tmpRepairer = RepairerRepository.Find(idServ);

            Car tmpCar = new Car();
            tmpCar = CarRepository.Find(Int32.Parse(car));

            Client tmpClient = new Client();
            tmpClient = ClientRepository.Find(Int32.Parse(client));

            model.Repairer = tmpRepairer;
            model.Car = tmpCar;
            model.Client = tmpClient;
            model.RepairerID = model.Repairer.ID;

            if (model.Parts != null)
            {
                foreach (var tmpPart in model.Parts)
                {
                    var title = PartRepository.Find(tmpPart.PartID);
                    tmpPart.Title = title.Title;
                    tmpPart.WorkingOrderID = model.ID;
                }
            }

            if (model.Repairs != null)
            {
                foreach (var tmpRepair in model.Repairs)
                {
                    var title = RepairRepository.Find(tmpRepair.RepairID);
                    tmpRepair.Title = title.Title;
                    tmpRepair.WorkingOrderID = model.ID;
                }
            }

            WorkingOrderRepository.UpdateDTO(model);
            WorkingOrderRepository.Save();

            return RedirectToAction("Index");
        }

        public ActionResult Delete(int id)
        {
            WorkingOrderRepository.Delete(id);
            WorkingOrderRepository.Save();

            return RedirectToAction("Index");
        }

        public ActionResult GetClients()
        {
            var context = new CompaniesManagerDbContext();

            context.Configuration.ProxyCreationEnabled = false;

            var possibleClients = context.Set<Client>().AsQueryable();
            // Get all categories using entity framework and LINQ queries.

            return Json(possibleClients, JsonRequestBehavior.AllowGet);
        }

        // GET: WorkingOrders/GetClientCars/2
        public String GetClientCars(int id)
        {
            var context = new CompaniesManagerDbContext();

            context.Configuration.ProxyCreationEnabled = false;

            var clientCars = context.Cars.Where(p => p.ClientID == id).ToList();



            //var json = JsonConvert.SerializeObject(clientCars);

            var json = JsonConvert.SerializeObject(clientCars, Formatting.Indented,
                              new JsonSerializerSettings
                              {
                                  ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                              });

            return json;

            //  return Json(cars, JsonRequestBehavior.AllowGet);


        }


        private void FillDropdownValuesRepairers()
        {
            var possibleRepairers = RepairerRepository.All.ToList();

            var selectItems = new List<SelectListItem>();

            var listItem = new SelectListItem(); //Polje je opcionalno
            //listItem.Text = "- odaberite -";
            //listItem.Value = "";
            //selectItems.Add(listItem);

            foreach (var repairer in possibleRepairers)
            {
                listItem = new SelectListItem();
                listItem.Text = repairer.Name + " " + repairer.Surname;
                listItem.Value = repairer.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }
            ViewBag.PossibleRepairers = selectItems;
        }

        private void FillDropdownValuesParts()
        {
            var possibleParts = PartRepository.All.ToList();

            var selectItems = new List<SelectListItem>();

            var listItem = new SelectListItem(); //Polje je opcionalno
            listItem.Text = "- odaberite -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var part in possibleParts)
            {
                listItem = new SelectListItem();
                listItem.Text = part.Title;
                listItem.Value = part.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }
            ViewBag.PossibleParts = selectItems;
        }

        private void FillDropdownValuesRepairs()
        {
            var possibleRepair = RepairRepository.All.ToList();

            var selectItems = new List<SelectListItem>();

            var listItem = new SelectListItem(); //Polje je opcionalno
            listItem.Text = "- odaberite -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var repair in possibleRepair)
            {
                listItem = new SelectListItem();
                listItem.Text = repair.Title;
                listItem.Value = repair.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }
            ViewBag.possibleRepair = selectItems;
        }

        private void FillDropdownValuesClients()
        {
            var possibleClients = ClientRepository.All.ToList();

            var selectItems = new List<SelectListItem>();

            var listItem = new SelectListItem(); //Polje je opcionalno
            listItem.Text = "- odaberite -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var client in possibleClients)
            {
                listItem = new SelectListItem();
                listItem.Text = client.Name + " " + client.Surname;
                listItem.Value = client.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }
            ViewBag.PossibleClients = selectItems;
        }

        [HttpPost]
        public ActionResult AddNewPart()
        {
            FillDropdownValuesParts();
            return PartialView("_SinglePartEdit", new PartDTO());
        }

        [HttpPost]
        public ActionResult AddNewRepair()
        {
            FillDropdownValuesRepairs();
            return PartialView("_SingleRepairEdit", new RepairDTO());
        }
    }
}
