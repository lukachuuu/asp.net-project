﻿using Ninject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.DAL;

namespace Vjezba.Web.Controllers
{
    public class VjezbaControllerBase : Controller
    {
      
        [Inject]
        public RepairRepository RepairRepository { get; set; }

        [Inject]
        public CarRepository CarRepository { get; set; }

        [Inject]
        public PartRepository PartRepository { get; set; }

        [Inject]
        public ClientRepository ClientRepository { get; set; }

        [Inject]
        public RepairerRepository RepairerRepository { get; set; }

        [Inject]
        public WorkingOrderRepository WorkingOrderRepository { get; set; }
    }
}