﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Model;

namespace Vjezba.Web.Controllers
{
    public class RepairController : VjezbaControllerBase
    {
        // GET: Repair
        [Authorize]
        public ActionResult Index()
        {
            var model = RepairRepository.All.ToList();
            return View(model);
        }

        [Authorize]
        public ActionResult Details(int id)
        {
            var model = RepairRepository.Find(id);

            return View(model);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create(Repair model)
        {
            if (ModelState.IsValid)
            {
                RepairRepository.Insert(model);
                RepairRepository.Save();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int id)
        {
            var model = RepairRepository.Find(id);

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(Repair formModel)
        {
            if (ModelState.IsValid)
            {
                var modelDb = RepairRepository.Find(formModel.ID);
                if (this.TryUpdateModel(modelDb))
                {
                    RepairRepository.Update(modelDb);
                    RepairRepository.Save();

                    return RedirectToAction("Index");
                }
            }

            return View(formModel);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int id)
        {
            RepairRepository.Delete(id);
            RepairRepository.Save();

            return RedirectToAction("Index");
        }
    }
}