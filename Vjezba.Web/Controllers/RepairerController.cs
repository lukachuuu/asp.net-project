﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Model;

namespace Vjezba.Web.Controllers
{
    [RoutePrefix("serviser")]
    public class RepairerController : VjezbaControllerBase
    {
        // GET: Part
        [Route("svi")]
        [Authorize]
        public ActionResult Index()
        {
            var model = RepairerRepository.All.ToList();

            return View(model);
        }

        [Authorize]
        public ActionResult Details(int id)
        {
            var model = RepairerRepository.Find(id);

            return View(model);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create(Repairer model)
        {
            if (ModelState.IsValid)
            {
                RepairerRepository.Insert(model);
                RepairerRepository.Save();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int id)
        {

            var model = RepairerRepository.Find(id);

            return View(model);
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(Repairer formModel)
        {
            if (ModelState.IsValid)
            {
                var modelDb = RepairerRepository.Find(formModel.ID);
                if (this.TryUpdateModel(modelDb))
                {
                    RepairerRepository.Update(modelDb);
                    RepairerRepository.Save();

                    return RedirectToAction("Index");
                }
            }
            return View(formModel);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int id)
        {
            RepairerRepository.Delete(id);
            RepairerRepository.Save();

            return RedirectToAction("Index");
        }
    }
}