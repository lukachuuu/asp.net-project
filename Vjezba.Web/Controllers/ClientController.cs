﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.DAL;
using Vjezba.Model;

namespace Vjezba.Web.Controllers
{
    public class ClientController : VjezbaControllerBase
    {
        // GET: Client
        public ActionResult Index()
        {
            var model = ClientRepository.All.ToList();

            return View(model);
        }

        [Authorize]
        [Route("klijent/detalji/{id}")]
        public ActionResult Details(int id)
        {
            var model = ClientRepository.Find(id);

            return View(model);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create()
        {
            return View(new ClientDTO());
        }

        [HttpPost]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Create(ClientDTO model)
        {
            if (ModelState.IsValid)
            {
                ClientRepository.InsertDTO(model);
                ClientRepository.Save();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Edit(int id)
        {

            var model = ClientRepository.GetDTO(id);

            return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        [Authorize(Roles = "Admin,Manager")]
        public ActionResult EditPost(ClientDTO formModel)
        {
            if (ModelState.IsValid)
            {
                var modelDb = ClientRepository.GetDTO(formModel.Id);
                if (this.TryUpdateModel(modelDb))
                {
                    ClientRepository.UpdateDTO(modelDb);
                    ClientRepository.Save();

                    return RedirectToAction("Index");
                }
            }

            return View(formModel);
        }

        [Authorize(Roles = "Admin,Manager")]
        public ActionResult Delete(int id)
        {
            ClientRepository.Delete(id);
            ClientRepository.Save();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult AddNewCar()
        {
            return PartialView("_SingleCarEdit", new CarDTO());
        }
    }
}