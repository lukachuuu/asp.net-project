﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Vjezba.Web.Areas.Admin.Controllers
{

    [RouteArea("Admin", AreaPrefix = "Administracija")]
    [RoutePrefix("Index")]
    public class AdminController : Controller
    {

        
        public ActionResult Index()
        {

            return View();
        }
    }
}